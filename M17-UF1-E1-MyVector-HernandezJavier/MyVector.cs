﻿using System;
using System.Collections.Generic;
using System.Text;

namespace M17_UF1_E1_MyVector_HernandezJavier
{
    class MyVector
    {
        public static int[,] vector;

        public static void iniciar()
        {
            bool showMenu = true;
            while (showMenu)
            {
                showMenu = MainMenu();
            }
        }


        private static bool MainMenu()
        {
            Console.Clear();
            Console.WriteLine("Escolleix una opcio: ");
            Console.WriteLine("1.- Crear vector");
            Console.WriteLine("2.- Mostrar vector");
            Console.WriteLine("3.- Inverteix vector");
            Console.WriteLine("4.- Sortir");
            Console.Write("\r\nSelect an option: ");

            switch (Console.ReadLine())
            {
                case "1":
                    creaVector();
                    return true;
                case "2":
                    mostraVector(vector);
                    return true;
                case "3":
                    rotarVector(vector);
                    return true;
                case "4":
                    return false;
                default:
                    return true;
            }
        }
        private static void creaVector()
        {
            int files = valor("Files");
            int columnes = valor("Columnes");
            Console.WriteLine("Crear vector random amb {0} files, {1} columnes", files, columnes);
            vector = new int[files, columnes];
            omplenaRandomVector(vector);
            Console.ReadLine();
        }

        static int valor(string nom) 
        {
            Console.Write("Entra el valor de {0}",nom);
            int n;

            while (!int.TryParse(Console.ReadLine(), out n))
            {
                Console.Clear();
                Console.WriteLine("Nombre invalid");
                Console.Write("Entra el valor de {0}", nom);
            }
            return n;
        }

        static int[,] omplenaRandomVector(int[,] vec) {
            Random rnd = new Random();
            for (int i = 0; i < vec.GetLength(0); i++)
            {
                for (int j = 0; j < vec.GetLength(1); j++)
                {
                    vec[i, j] = rnd.Next(0, 9);
                }
            }
            return vec;
        }

        private static void mostraVector(int[,] vec) 
        {
			for (int i = 0; i < vec.GetLength(0); i++)
			{
				for (int j = 0; j < vec.GetLength(1); j++)
				{
                    Console.Write(vec[i, j]);
				}
                Console.WriteLine();
			}
            Console.ReadLine();
        }

        private static void rotarVector(int[,] vectorDonat) 
        {
			for (int i = vectorDonat.GetLength(0)-1; i >= 0; i--)
			{
				for (int j = vectorDonat.GetLength(1)-1; j >= 0; j--)
				{
                    Console.Write(vectorDonat[i, j]);
                }
                Console.WriteLine();
            }
        }
    }
}
